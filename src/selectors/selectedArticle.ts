import { createSelector } from 'reselect';
import { IArticle } from '../redux/article/article-type';
import { RootState } from '../redux/root-reducer';

const getKeyword = (state: RootState) => state.articles.filter;
const articleData = (state: RootState) => state.articles.articles;

export const filterArticleSelector = createSelector(
  [articleData, getKeyword],
  (articleData, getKeyword) => {
    articleData&& articleData.filter(
      (article: IArticle) => article.title.trim()
        .toLowerCase()
        .includes(getKeyword)
    )
  }
);
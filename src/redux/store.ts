import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { rootReducer } from './root-reducer'
import { composeWithDevTools } from 'redux-devtools-extension';
// import { reduxFirestore, getFirestore } from 'redux-firestore'
// import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
const middleware= [thunk]

export const store = createStore(
  rootReducer,
  composeWithDevTools(
    (applyMiddleware(...middleware))
  )
)
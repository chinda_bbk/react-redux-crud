import { ArticleActionTypes } from './article-action-type';

export interface IArticle {
  body: string;
  createdAt: any;
  id: string;
  title: string;
  userId: string;
  username: string;
  comment: number;
}

export interface IArticleState {
  articles: IArticle[];
  article: IArticle;
  filter: string;
  searched: IArticle[];
  lastArticle: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >;
  firstArticle: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >;
  nextStep: boolean;
  prevStep: boolean;
  users: string[];
  owner: boolean;
}

export interface IAddArticleData {
  body: string;
  title: string;
}

export interface IArticleData {
  body: string;
  id: string;
  title: string;
}

interface IRetreiveArticle {
  type: ArticleActionTypes.RETREIVE_ARTICLE;
  payload: IArticle[];
}
interface IAddArticle {
  type: ArticleActionTypes.ADD_ARTICLE;
  payload: IArticle;
}
interface IRemoveArticle {
  type: ArticleActionTypes.REMOVE_ARTICLE;
}
interface IUpdateArticle {
  type: ArticleActionTypes.UPDATE_ARTICLE;
}
interface IRetreiveSingleArticle {
  type: ArticleActionTypes.RETREIVE_SINGLE_ARTICLE;
  payload: IArticle;
}
interface IFilterArticle {
  type: ArticleActionTypes.FILTER_ARTICLE;
  payload: string;
}
interface ISearchArticle {
  type: ArticleActionTypes.SEARCH_ARTICLE;
  payload: IArticle[];
}
interface ILastArticle {
  type: ArticleActionTypes.LAST_ARTICLE;
  payload: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >;
}
interface IFirstArticle {
  type: ArticleActionTypes.FIRST_ARTICLE;
  payload: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >;
}
interface IRetreiveNextArticle {
  type: ArticleActionTypes.RETREIVE_NEXT_ARTICLE;
  payload: IArticle[];
}
interface IRetreivePrevArticle {
  type: ArticleActionTypes.RETREIVE_PREV_ARTICLE;
  payload: IArticle[];
}
interface INextStep {
  type: ArticleActionTypes.NEXT_STEP;
  payload: boolean;
}
interface IPrevStep {
  type: ArticleActionTypes.PREV_STEP;
  payload: boolean;
}
interface IResetSearch {
  type: ArticleActionTypes.RESET_SEARCH;
}
interface IGetUsers {
  type: ArticleActionTypes.GET_USERS;
  payload: string[]
}
interface ICheckOwner {
  type: ArticleActionTypes.CHECK_OWNER;
  payload: boolean
}

export type ArticleAction =
  | IRetreiveArticle
  | IAddArticle
  | IRemoveArticle
  | IUpdateArticle
  | IRetreiveSingleArticle
  | ILastArticle
  | IFirstArticle
  | ISearchArticle
  | IRetreiveNextArticle
  | IRetreivePrevArticle
  | INextStep
  | IPrevStep
  | IFilterArticle
  | IResetSearch
  | IGetUsers
  | ICheckOwner;

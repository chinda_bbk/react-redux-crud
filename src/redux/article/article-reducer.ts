import { Reducer } from 'redux'
import { ArticleActionTypes } from './article-action-type'
import { ArticleAction, IArticleState } from './article-type'

const initialState: IArticleState = {
  articles: null!,
  article: null!,
  filter: null!,
  searched: null!,
  lastArticle: null!,
  firstArticle: null!,
  nextStep: false,
  prevStep: true,
  users: null!,
  owner: false
}

export const articleReducer: Reducer<IArticleState, ArticleAction> = (state: IArticleState = initialState, action: ArticleAction): IArticleState => {
  switch (action.type) {
    case ArticleActionTypes.ADD_ARTICLE:
      return {
        ...state
      }

    case ArticleActionTypes.REMOVE_ARTICLE:
      return {
        ...state
      }

    case ArticleActionTypes.UPDATE_ARTICLE:
      return {
        ...state
      }

    case ArticleActionTypes.RETREIVE_ARTICLE:
      return {
        ...state,
        articles: action.payload
      }

    case ArticleActionTypes.RETREIVE_SINGLE_ARTICLE:
      return {
        ...state,
        article: action.payload
      }

    case ArticleActionTypes.FILTER_ARTICLE:
      return {
        ...state,
        filter: action.payload
      }

    case ArticleActionTypes.SEARCH_ARTICLE:
      return {
        ...state,
        searched: action.payload
      }

    case ArticleActionTypes.LAST_ARTICLE:
      return {
        ...state,
        lastArticle: action.payload
      }

    case ArticleActionTypes.FIRST_ARTICLE:
      return {
        ...state,
        firstArticle: action.payload
      }

    case ArticleActionTypes.RETREIVE_NEXT_ARTICLE:
      return {
        ...state,
        articles: action.payload
      }

    case ArticleActionTypes.RETREIVE_PREV_ARTICLE:
      return {
        ...state,
        articles: action.payload
      }

    case ArticleActionTypes.NEXT_STEP:
      return {
        ...state,
        nextStep: action.payload
      }

    case ArticleActionTypes.PREV_STEP:
      return {
        ...state,
        prevStep: action.payload
      }

    case ArticleActionTypes.RESET_SEARCH:
      return {
        ...state,
        searched: null!
      }

    case ArticleActionTypes.GET_USERS:
      return {
        ...state,
        users: action.payload
      }

    case ArticleActionTypes.CHECK_OWNER:
      return {
        ...state,
        owner: action.payload
      }

    default:
      return state
  }
}
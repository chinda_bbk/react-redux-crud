import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { RootState } from '../root-reducer';
import { ArticleActionTypes } from './article-action-type';
import {
  IAddArticleData,
  IArticleData,
  ArticleAction,
  IArticle,
} from './article-type';
import firebase from '../../firebase/config';
const db = firebase.firestore();

export const addArticle = (
  data: IAddArticleData
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch, getState) => {
    try {
      const id = db.collection('articles').doc().id;
      const uId = firebase.auth().currentUser?.uid;
      const username = getState().auth.user.firstName;
      const article = {
        title: data.title,
        body: data.body,
        id: id,
        userId: uId,
        username,
        comment: 0,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      };
      await db.collection('articles').doc(id).set(article);
      dispatch({
        type: ArticleActionTypes.ADD_ARTICLE,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const removeArticle = (
  data: IArticleData
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch<any>) => {
    let article = await db.doc(`articles/${data.id}`).get();
    if (!article.exists) {
      alert('fail...');
      return false;
    }
    const batch = db.batch();
    let comments = await article.ref.collection('comments').get();
    comments.docs.forEach((cmtDoc) => {
      batch.delete(cmtDoc.ref);
    });
    batch.delete(article.ref);
    batch.commit();
    dispatch(receiveArticle());
    dispatch({
      type: ArticleActionTypes.REMOVE_ARTICLE,
    });
    // dispatch(receiveArticle())
  };
};

// --- Batched delete ---
// const batchDel = async (querySnap: firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>) => {
//   querySnap.forEach(artDoc => {
//     const batch = db.batch()

//     let snapshots = await artDoc.ref.collection('comments').get();

//     snapshots.docs.forEach(cmtDoc => {
//       // Add doc to batch
//       batch.delete(cmtDoc.ref)
//     })
//     // Commit the batch
//     // artDoc.ref.delete()
//     batch.delete(artDoc.ref)
//     batch.commit()
//   })
// }

export const updateArticle = (
  data: IArticleData
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch<any>) => {
    await db.collection('articles')
      .where('id', '==', data.id)
      .get()
      .then((querySnap) => {
        querySnap.forEach((doc) => {
          doc.ref.update(data);
        });
      });
    dispatch(retreiveSingleArticle(data.id));
    dispatch({
      type: ArticleActionTypes.UPDATE_ARTICLE,
    });
  };
};

export const receiveArticle = (): ThunkAction<
  void,
  RootState,
  null,
  ArticleAction
> => {
  return async (dispatch: Dispatch) => {
    try {
      // db.collection('articles').onSnapshot(snapShot => {
      //   const getArticles = snapShot.docs.map((doc) => doc.data()) as IArticle[]
      //   dispatch({
      //     type: ArticleActionTypes.RETREIVE_ARTICLE,
      //     payload: getArticles
      //   })
      // })

      await db.collection('articles')
        .orderBy('createdAt', 'desc')
        .limit(4)
        .get()
        .then((res) => {
          const articles = res.docs.map((doc) => doc.data()) as IArticle[];
          const lastArticle = res.docs[res.docs.length - 1];
          const fisrtArticle = res.docs[res.docs.length - 4];
          dispatch({
            type: ArticleActionTypes.LAST_ARTICLE,
            payload: lastArticle,
          });
          dispatch({
            type: ArticleActionTypes.FIRST_ARTICLE,
            payload: fisrtArticle,
          });
          dispatch({
            type: ArticleActionTypes.RETREIVE_ARTICLE,
            payload: articles,
          });
        });
    } catch (err) {
      console.log(err.message);
      dispatch({
        type: ArticleActionTypes.RETREIVE_ARTICLE,
        payload: [],
      });
    }
  };
};

export const getNextArticle = (
  lastArticle: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch, getState) => {
    let pagesize = 4;
    let field = 'createdAt';
    try {
      if (lastArticle !== undefined)
        await db.collection('articles')
          .orderBy(field, 'desc')
          .startAfter(lastArticle)
          .limit(pagesize)
          .get()
          .then((res) => {
            if (res.size) {
              const articles = res.docs.map((doc) => doc.data()) as IArticle[];
              const lastArticle = res.docs[res.docs.length - 1];
              const firstArticle = res.docs[res.docs.length - 4];
              if (res.size < 4) {
                dispatch({
                  type: ArticleActionTypes.PREV_STEP,
                  payload: false,
                });
                dispatch({
                  type: ArticleActionTypes.NEXT_STEP,
                  payload: true,
                });
                dispatch({
                  type: ArticleActionTypes.FIRST_ARTICLE,
                  payload: res.docs[0],
                });
                dispatch({
                  type: ArticleActionTypes.RETREIVE_NEXT_ARTICLE,
                  payload: articles,
                });
                dispatch({
                  type: ArticleActionTypes.PREV_STEP,
                  payload: false,
                });
                return;
              }

              dispatch({
                type: ArticleActionTypes.PREV_STEP,
                payload: false,
              });
              dispatch({
                type: ArticleActionTypes.NEXT_STEP,
                payload: lastArticle === undefined ? true : false,
              });
              dispatch({
                type: ArticleActionTypes.LAST_ARTICLE,
                payload: lastArticle,
              });
              dispatch({
                type: ArticleActionTypes.FIRST_ARTICLE,
                payload: firstArticle,
              });
              dispatch({
                type: ArticleActionTypes.RETREIVE_NEXT_ARTICLE,
                payload: articles,
              });
            } else {
              dispatch({
                type: ArticleActionTypes.NEXT_STEP,
                payload: true,
              });
              dispatch({
                type: ArticleActionTypes.PREV_STEP,
                payload: false,
              });
            }
          });
    } catch (err) {
      console.log(err.message);
      dispatch({
        type: ArticleActionTypes.RETREIVE_ARTICLE,
        payload: [],
      });
    }
  };
};

export const getPrevArticle = (
  firstArticle: firebase.firestore.QueryDocumentSnapshot<
    firebase.firestore.DocumentData
  >
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch, getState) => {
    let pagesize = 4;
    let field = 'createdAt';
    try {
      if (firstArticle === undefined) {
        dispatch({
          type: ArticleActionTypes.PREV_STEP,
          payload: true,
        });
        dispatch({
          type: ArticleActionTypes.FIRST_ARTICLE,
          payload: getState().articles.lastArticle,
        });
      }
      if (firstArticle !== undefined)
        await db.collection('articles')
          .orderBy(field, 'desc')
          .endBefore(firstArticle)
          .limitToLast(pagesize)
          .get()
          .then((res) => {
            if (res.size) {
              const articles = res.docs.map((doc) => doc.data()) as IArticle[];
              const lastArticle = res.docs[res.docs.length - 1];
              const firstAr = res.docs[res.docs.length - 4];
              dispatch({
                type: ArticleActionTypes.PREV_STEP,
                payload: false,
              });
              dispatch({
                type: ArticleActionTypes.NEXT_STEP,
                payload: false,
              });
              dispatch({
                type: ArticleActionTypes.LAST_ARTICLE,
                payload: lastArticle,
              });
              dispatch({
                type: ArticleActionTypes.FIRST_ARTICLE,
                payload: firstAr,
              });
              dispatch({
                type: ArticleActionTypes.RETREIVE_PREV_ARTICLE,
                payload: articles,
              });
            } else {
              dispatch({
                type: ArticleActionTypes.PREV_STEP,
                payload: true,
              });
              dispatch({
                type: ArticleActionTypes.NEXT_STEP,
                payload: false,
              });
            }
          });
    } catch (err) {
      console.log(err.message);
      dispatch({
        type: ArticleActionTypes.RETREIVE_ARTICLE,
        payload: [],
      });
    }
  };
};

export const retreiveSingleArticle = (
  id: string
): ThunkAction<void, RootState, null, ArticleAction> => {
  return async (dispatch: Dispatch) => {
    const articleRef = db.collection('articles').doc(id);
    await articleRef.get().then((doc) => {
      if (doc.exists) {
        dispatch({
          type: ArticleActionTypes.RETREIVE_SINGLE_ARTICLE,
          payload: doc.data(),
        });
      } else {
        dispatch({
          type: ArticleActionTypes.RETREIVE_SINGLE_ARTICLE,
          payload: [],
        });
      }
    }).catch((error) => {
      console.log('Error getting cached document:', error);
    });
  };
};

export const filterArticles = (
  title: string
): ThunkAction<void, RootState, null, ArticleAction> => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: ArticleActionTypes.FILTER_ARTICLE,
      payload: title.trim().toLowerCase(),
    });
  };
};

export const searchArticle = (
  title: string,
  dateStart: string,
  dateEnd: string,
  username: string
): ThunkAction<void, RootState, null, ArticleAction> => {
  return (dispatch: Dispatch) => {
    const collRef = db.collection('articles');
    const titleValid = title.trim().toLowerCase()
    const startOfDay = firebase.firestore.Timestamp.fromDate(new Date(dateStart))
    const endOfDay = firebase.firestore.Timestamp.fromDate(new Date(dateEnd))
    try {
      collRef
        .where('title', '==', titleValid)
        .where('createdAt', '>=', startOfDay)
        .where('createdAt', '<=', endOfDay)
        .where('username', '==', username)
        .orderBy('createdAt')
        .onSnapshot((snapshot) => {
          let articles: IArticle[] = [];
          snapshot.forEach((doc) => {
            const data: IArticle | any = doc.data();
            articles = [...articles, data];
          });
          dispatch({
            type: ArticleActionTypes.SEARCH_ARTICLE,
            payload: articles,
          });
        })
    } catch (error) {
      console.log(error)
    }
  };
};

export const resetSearch = (): ThunkAction<void, RootState, null, ArticleAction> => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: ArticleActionTypes.RESET_SEARCH
    })
  }
}

export const getUsers = (): ThunkAction<void, RootState, null, ArticleAction> => {
  return (dispatch: Dispatch) => {
    try {
      db.collection('users').onSnapshot((snap) => {
        let users: string[] = [];
        snap.forEach(doc => {
          users.push(doc.data().firstName)
        })
        dispatch({
          type: ArticleActionTypes.GET_USERS,
          payload: users
        })
      })
    } catch (error) {
      console.log(error)
    }
  }
}

export const checkOwner = (userId: string, articleId: string): ThunkAction<void, RootState, null, ArticleAction> => {
  return (dispatch: Dispatch) => {
    let owner: boolean = false;
    if (userId === articleId) owner = true;
    else owner = false;
    dispatch({
      type: ArticleActionTypes.CHECK_OWNER,
      payload: owner
    })
  }
}
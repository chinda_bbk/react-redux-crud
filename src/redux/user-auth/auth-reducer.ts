import { AuthState } from './auth-type'
import { ActionType, AuthActionType } from './auth-action-type'
import { Reducer } from 'redux'

const initialState: AuthState = {
  user: null!,
  authenticated: false,
  loading: false,
  error: '',
  needVerification: false,
  success: ''
}

export const authReducer: Reducer<AuthState, AuthActionType> = (state = initialState, action: AuthActionType): AuthState => {
  switch (action.type) {
    case ActionType.SET_USER:
      return {
        ...state,
        user: action.payload,
        authenticated: true
      }

    case ActionType.SET_LOADING:
      return {
        ...state,
        loading: action.payload
      }

    case ActionType.SIGN_OUT:
      return {
        ...state,
        user: null!,
        authenticated: false,
        loading: false
      }

    case ActionType.SET_ERROR:
      return {
        ...state,
        error: action.payload
      }

    case ActionType.NEED_VERIFICATION:
      return {
        ...state,
        needVerification: true
      }

    case ActionType.SET_SUCCESS:
      return {
        ...state,
        success: action.payload
      }

    default:
      return state;
  }
}
import { ThunkAction } from 'redux-thunk'
import { User, SignUpData, SignInData } from './auth-type'
import { ActionType, AuthActionType } from './auth-action-type'
import { RootState } from '../root-reducer'
import firebase from '../../firebase/config'

// Create user
export const signup = (data: SignUpData, onError: () => void): ThunkAction<void, RootState, null, AuthActionType> => {
  return async (dispatch) => {
    try {
      const res = await firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
      if(res.user) {
        const userData: User = {
          email: data.email,
          firstName: data.firstName,
          id: res.user.uid,
          // https://firebase.google.com/docs/reference/js/firebase.firestore.FieldValue
          createdAt: firebase.firestore.FieldValue.serverTimestamp()
        }
        await firebase.firestore().collection('/users').doc(res.user.uid).set(userData)
        await res.user.sendEmailVerification()
        dispatch({
          type: ActionType.NEED_VERIFICATION
        })
        dispatch({
          type: ActionType.SET_USER,
          payload: userData
        })
      }
    } catch (err) {
      console.log(err)
      onError()
      switch(err.code) {
        case "auth/email-already-in-use":
        case 'auth/invalid-email':
          dispatch({
            type: ActionType.SET_ERROR,
            payload: err.message
          })
          break
        case 'auth/weak-password':
          dispatch({
            type: ActionType.SET_ERROR,
            payload: err.message
          })
          break
        default:
          break
      }
    }
  }
}

export const getUserById = (id: string): ThunkAction<void, RootState, null, AuthActionType> => {
  return async dispatch => {
    try {
      const user = await firebase.firestore().collection('/users').doc(id).get()
      if(user.exists) {
        // Tells compiler that userData as obj User
        const userData = user.data() as User
        dispatch({
          type: ActionType.SET_USER,
          payload: userData
        })
      }
    } catch (err) {
      console.log(err.message)
    }
  }
}

export const setLoading = (value: boolean): ThunkAction<void, RootState, null, AuthActionType> => {
  return async dispatch => {
    dispatch({
      type: ActionType.SET_LOADING,
      payload: value
    })
  }
}

export const signin = (data: SignInData, onError: () => void): ThunkAction<void, RootState, null, AuthActionType> => {
  return async dispatch => {
    try {
      await firebase.auth().signInWithEmailAndPassword(data.email, data.password)
    } catch (err) {
      onError()
      switch(err.code) {
        case "auth/invalid-email":
        case 'auth/user-disabled':
        case 'auth/user-not-found':
          dispatch(setError(err.message))
          break
        case 'auth/wrong-password':
          dispatch(setError(err.message))
          break
        default:
          break
      }
    }
  }
}

export const signout = (): ThunkAction<void, RootState, null, AuthActionType> => {
  return async dispatch => {
    try {
      dispatch(setLoading(true))
      await firebase.auth().signOut()
      dispatch({
        type: ActionType.SIGN_OUT
      })
    } catch (err) {
      console.log(err)
      dispatch(setLoading(false))
    }
  }
}

export const setError = (msg: string): ThunkAction<void, RootState, null, AuthActionType> => {
  return dispatch => {
    dispatch({
      type: ActionType.SET_ERROR,
      payload: msg
    })
  }

}

export const setNeedVerification = (): ThunkAction<void, RootState, null, AuthActionType> => {
  return dispatch => {
    dispatch({
      type: ActionType.NEED_VERIFICATION
    });
  }
}

export const setSuccess = (msg: string): ThunkAction<void, RootState, null, AuthActionType> => {
  return dispatch => {
    console.log(msg)
    dispatch({
      type: ActionType.SET_SUCCESS,
      payload: msg
    })
  }
}

export const sendPasswordResetEmail = (email: string, successMsg: string): ThunkAction<void, RootState, null, AuthActionType> => {
  return async dispatch => {
    try {
      await firebase.auth().sendPasswordResetEmail(email)
      dispatch(setSuccess(successMsg))
    } catch (err) {
      switch(err.code) {
        case "auth/invalid-email":
        case 'auth/user-disabled':
        case 'auth/user-not-found':
          dispatch(setError(err.message))
          break
        default:
          break
      }
    }
  }
}
// import { ActionType } from "./auth-action-type";

export interface User {
  firstName: string
  email: string
  id: string
  createdAt: any
}

export interface AuthState {
  user: User
  authenticated: boolean
  loading: boolean
  error: string
  needVerification: boolean
  success: string
}

export interface SignUpData {
  firstName: string
  email: string
  password: string
}

export interface SignInData {
  email: string
  password: string
}

// Actions
// interface SetUserAction {
//   type: typeof ActionType.SET_USER
//   payload: User
// }

// interface SetLoadingAction {
//   type: typeof ActionType.SET_LOADING
//   payload: boolean
// }

// interface SignOutAction {
//   type: typeof ActionType.SIGN_OUT
// }

// interface SetErrorAction {
//   type: typeof ActionType.SET_ERROR
//   payload: string
// }

// interface NeedVerificationAction {
//   type: typeof ActionType.NEED_VERIFICATION
// }

// interface SetSuccessAction {
//   type: typeof ActionType.SET_SUCCESS
//   payload: string
// }

// export type AuthAction = 
//   SetUserAction | 
//   SetLoadingAction | 
//   SignOutAction | 
//   SetErrorAction |
//   NeedVerificationAction |
//   SetSuccessAction; 
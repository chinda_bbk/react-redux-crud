import {User} from './auth-type'

export enum ActionType {
  SET_USER = 'SET_USER',
  SIGN_OUT = 'SIGN_OUT',
  SET_LOADING = 'SET_LOADING',
  SET_ERROR = 'SET_ERROR',
  NEED_VERIFICATION = 'NEED_VERIFICATION',
  SET_SUCCESS = 'SET_SUCCESS'
}

export type AuthActionType =
| {type: typeof ActionType.SET_USER, payload: User}
| {type: typeof ActionType.SET_SUCCESS, payload: string}
| {type: typeof ActionType.SET_LOADING, payload: boolean}
| {type: typeof ActionType.SET_ERROR, payload: string}
| {type: typeof ActionType.SIGN_OUT}
| {type: typeof ActionType.NEED_VERIFICATION}
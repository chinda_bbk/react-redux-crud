import { combineReducers } from 'redux'
import { articleReducer } from './article/article-reducer'
import { commentReducer } from './comment/comment-reducer'
import { authReducer } from './user-auth/auth-reducer'
// import { useSelector, TypedUseSelectorHook } from 'react-redux'

export const rootReducer = combineReducers({
  articles: articleReducer,
  auth: authReducer,
  comments: commentReducer
})

export type RootState = ReturnType<typeof rootReducer>

// export const TypeUseSelector: TypedUseSelectorHook<RootState> = useSelector;
// const a = TypeUseSelector(state => state.articles)

// export interface RootState1{
//   article:IArticle
// }

// const s1 =(state:RootState)=> state.article
// const s2 =(state:RootState1)=> state.article
// const selector = useSelector(s1)

// const selector1 = useSelector((state:RootState)=>state.articles)
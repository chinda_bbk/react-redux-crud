export interface CommentType {
  content: string
  username: string
  articleId: string
  id: string
  createdAt: any
  userId: string
}

export interface CommentState {
  comments: CommentType[]
}

export interface AddCommentData {
  content: string
  username: string
  articleId: string
}

export interface EditCommentData {
  content: string
  id: string
  articleId: string
}

export interface DeleteCommentData {
  id: string
  articleId: string
}
import { Dispatch } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { AddCommentData, EditCommentData, DeleteCommentData } from './comment-type'
import { ActionTypes, CommentActionType } from './comment-action-type'
import { RootState } from '../root-reducer'
import firebase from '../../firebase/config'
const db = firebase.firestore()

export const addComment = (data: AddCommentData): ThunkAction<void, RootState, null, CommentActionType> => {
  return async (dispatch: Dispatch) => {
    try {
      const idRef = db.collection('articles').doc(data.articleId).collection('comments').doc().id
      const uId = firebase.auth().currentUser?.uid
      const comment = {
        content: data.content,
        username: data.username,
        articleId: data.articleId,
        id: idRef,
        userId: uId,
        createdAt: firebase.firestore.FieldValue.serverTimestamp()
      }
      let article = await db.doc(`articles/${data.articleId}`).get();

      if (article.exists) {
        await db.doc(`articles/${data.articleId}/comments/${idRef}`).set(comment);
        dispatch({
          type: ActionTypes.ADD_COMMENT
        })
      } else {
        console.log('Access denied...')
      }
    } catch (err) {
      console.log(err)
    }
  }
}

export const setComments = (articleId: string): ThunkAction<void, RootState, null, CommentActionType> => {
  return async (dispatch: Dispatch) => {
    try {
      db.collection('articles').doc(articleId).collection('comments').orderBy("createdAt", "desc").onSnapshot(snapshot => {
        const comments = snapshot.docs.map(doc => doc.data())
        dispatch({
          type: ActionTypes.SET_COMMENTS,
          payload: comments
        })
      })
    } catch (err) {
      console.log(err)
      dispatch({
        type: ActionTypes.SET_COMMENTS,
        payload: []
      })
    }
  }
}

export const editComment = (data: EditCommentData): ThunkAction<void, RootState, null, CommentActionType> => {
  return async (dispatch: Dispatch) => {
    try {
      await db.collection('articles').doc(data.articleId).collection('comments').where('id', '==', data.id).get().then(snapshot => {
        snapshot.forEach(doc => {
          doc.ref.update({ content: data.content })
        })
      })
      dispatch({
        type: ActionTypes.EDIT_COMMENT
      })
    } catch (err) {
      console.log(err)
    }
  }
}

export const removeComment = (data: DeleteCommentData): ThunkAction<void, RootState, null, CommentActionType> => {
  return async (dispatch: Dispatch) => {
    await db.collection('articles').doc(data.articleId).collection('comments').doc(data.id).delete().then(() => {
      console.log('Comment delete successfully')
    }).catch(err => {
      console.log(err.message)
    })
    dispatch({
      type: ActionTypes.REMOVE_COMMENT
    })
  }
}
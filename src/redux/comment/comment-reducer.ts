import { Reducer } from 'redux'
import { CommentState } from './comment-type'
import { ActionTypes, CommentActionType } from './comment-action-type'

const initialstate: CommentState = {
  comments: null!
}

export const commentReducer: Reducer<CommentState, CommentActionType> = (state: CommentState = initialstate, action: CommentActionType): CommentState => {
  switch (action.type) {
    case ActionTypes.ADD_COMMENT:
      return {
        ...state
      }
    case ActionTypes.SET_COMMENTS:
      return {
        ...state,
        comments: action.payload
      }
    case ActionTypes.REMOVE_COMMENT:
      return {
        ...state
      }
    case ActionTypes.EDIT_COMMENT:
      return {
        ...state
      }
    default:
      return state
  }
}
import { CommentType } from './comment-type'

export enum ActionTypes {
  ADD_COMMENT = 'ADD_COMMENT',
  REMOVE_COMMENT = 'REMOVE_COMMENT',
  EDIT_COMMENT = 'EDIT_COMMENT',
  SET_COMMENTS = 'SET_COMMENTS',
}

interface AddCommentAction {
  type: ActionTypes.ADD_COMMENT,
  payload: CommentType
}

interface SetCommentAction {
  type: ActionTypes.SET_COMMENTS, 
  payload: CommentType[]
}

interface RemoveCommentAction {
  type: ActionTypes.REMOVE_COMMENT
}

interface UpdateCommentAction {
  type: ActionTypes.EDIT_COMMENT
}

export type CommentActionType = 
  | AddCommentAction 
  | SetCommentAction 
  | RemoveCommentAction 
  | UpdateCommentAction
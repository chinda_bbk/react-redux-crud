import React, { FC, useCallback, useEffect, useState } from 'react';
import { Dispatch } from 'redux';
import { RootState } from '../../redux/root-reducer';
import { IArticle } from '../../redux/article/article-type';
import { useParams, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {
  updateArticle,
  retreiveSingleArticle,
  removeArticle,
  checkOwner,
} from '../../redux/article/article-action';
import EditArticle from './EditArticle';
import Comments from '../comment/Comments';
import moment from 'moment';

import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  Button,
  ButtonGroup,
  Typography,
  Modal,
  Backdrop,
} from '@material-ui/core';
import DeleteSweepIcon from '@material-ui/icons/DeleteSweep';
import EditIcon from '@material-ui/icons/Edit';

type Params = { id: string };

const SingleArticle: FC = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const history = useHistory();
  const { authenticated, user } = useSelector((state: RootState) => state.auth);
  const { article, owner } = useSelector((state: RootState) => state.articles);
  const userId = user && user.id;
  const articleId = article && article.userId;
  let params: Params = useParams();

  useEffect(() => {
    dispatch(checkOwner(userId, articleId));
    dispatch(retreiveSingleArticle(params.id));
  }, [dispatch, params.id, userId, articleId]);

  const editArticle = useCallback(
    (article: IArticle) => {
      dispatch(updateArticle(article));
      // dispatch(retreiveSingleArticle(params.id));
    },
    [dispatch]
  );

  const [openModal, setOpenModal] = useState(false);
  const [confirmModal, setConfirmModal] = useState(false);
  const handleModal = () => {
    setOpenModal(!openModal);
  };
  const handleCancel = () => {
    setConfirmModal(!confirmModal);
  };
  const handleYes = () => {
    dispatch(removeArticle(article));
    // dispatch(receiveArticle());
    history.push('/articles');
    setConfirmModal(false);
  };

  return (
    <div>
      <Card className='Card'>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant='h5' component='h2'>
              {article && article.title}
            </Typography>
            <Typography gutterBottom variant='subtitle2' component='h2'>
              Posted by {article && article.username} :{' '}
              {moment(article && article.createdAt.toDate()).calendar()}
            </Typography>
            <Typography variant='body2' component='p'>
              {article && ''}
            </Typography>
            <Typography variant='body2' component='p'>
              {article && article.body}
            </Typography>
          </CardContent>
        </CardActionArea>
        {authenticated && owner && (
          <CardActions className='Card-item'>
            <ButtonGroup>
              <Button size='small' color='secondary' onClick={handleCancel}>
                <DeleteSweepIcon />
              </Button>
              <Button size='small' color='primary' onClick={handleModal}>
                {openModal ? 'Cancel' : <EditIcon />}
              </Button>
            </ButtonGroup>
          </CardActions>
        )}
      </Card>
      <Comments articleId={params.id} />

      <Modal
        open={confirmModal}
        onClose={handleCancel}
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'
        className='Modal-wrapper'
      >
        <div className='Confirm-modal'>
          <Typography gutterBottom>Are you sure you 1 2 delete?</Typography>
          <div className='CM-action'>
            <Button size='small' onClick={handleCancel}>
              Cancel
            </Button>
            <Button size='small' color='secondary' onClick={handleYes}>
              Yes
            </Button>
          </div>
        </div>
      </Modal>

      <Modal
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'
        open={openModal}
        onClose={handleModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <div>
          <EditArticle
            oldArticle={article}
            editArticle={editArticle}
            handleModal={handleModal}
          />
        </div>
      </Modal>
    </div>
  );
};

export default SingleArticle;

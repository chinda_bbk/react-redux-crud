import React from 'react';
import { IArticle } from '../../redux/article/article-type'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

type Props = {
  oldArticle: IArticle;
  handleModal: () => void;
  editArticle: (article: IArticle) => void;
};

type HTMLMultipleElement = HTMLInputElement | HTMLTextAreaElement;

const EditArticle: React.FC<Props> = ({ oldArticle, handleModal, editArticle, }) => {
  const [article, setArticle] = React.useState<IArticle>(oldArticle);
  const handleArticleData = (e: React.FormEvent<HTMLMultipleElement>) => {
    setArticle({
      ...article,
      [e.currentTarget.name]: e.currentTarget.value,
    });
  };
  
  const updateNewArticle = (e: React.FormEvent) => {
    e.preventDefault();
    editArticle(article);
    handleModal();
  };

  function closeModal() {
    handleModal();
  }

  return (
    <div className="Modal">
      <form onSubmit={updateNewArticle} className="Form">
        <Typography variant="h5" className="Form-title"><span> Edit Article </span>
          <IconButton onClick={closeModal} color='secondary'>
            <CloseIcon />
          </IconButton>
        </Typography>
        <TextField
          focused
          variant='outlined'
          type='text'
          label='Title'
          name='title'
          value={article.title}
          onChange={handleArticleData}
        />
        <TextField
          multiline
          variant='outlined'
          type='text'
          label='Body'
          name='body'
          rows={10}
          value={article.body}
          onChange={handleArticleData}
        />
        <Button
          type='submit'
          variant='outlined'
          color='primary'
          disabled={article === undefined ? true : false}
        >
          Update Article
        </Button>
      </form>
    </div>
  );
};

export default EditArticle;

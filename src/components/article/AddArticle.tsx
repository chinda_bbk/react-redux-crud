import React from 'react'
import { IArticle } from '../../redux/article/article-type'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

type Props = {
  handleModal: any
  saveArticle: (article: IArticle | any) => void
}

type HTMLMultipleElement = HTMLInputElement | HTMLTextAreaElement

const AddArticle: React.FC<Props> = ({ saveArticle, handleModal }) => {
  const [article, setArticle] = React.useState<IArticle | {}>()

  const handleArticleData = (e: React.FormEvent<HTMLMultipleElement>) => {
    setArticle({
      ...article,
      [e.currentTarget.name]: e.currentTarget.value
    })
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    saveArticle(article)
    handleModal()
  }

  function closeModal() {
    handleModal()
  }

  return (
    <div className="Modal">
      <form onSubmit={handleSubmit} className="Form">
        <Typography variant="h5" className="Form-title"><span> Create New Article </span>
          <IconButton onClick={closeModal} color="secondary">
            <CloseIcon/>
          </IconButton>
        </Typography>
        <TextField focused variant="outlined" type="text" label="Title" name="title" onChange={handleArticleData} />
        <TextField multiline variant="outlined" label="Body" name="body" onChange={handleArticleData} rows={10}></TextField>
        <Button type="submit" variant="outlined" color="primary" disabled={article === undefined ? true : false}>Add Article</Button>
      </form>
    </div>
  )
}

export default AddArticle

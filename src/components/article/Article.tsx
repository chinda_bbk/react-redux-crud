import React, { FC } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { IArticle } from '../../redux/article/article-type';
import moment from 'moment';

import Link from '@material-ui/core/Link';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

type Props = { article: IArticle };

const Article: FC<Props> = ({ article }) => {
  return (
    <div>
      <Card className='Card'>
        <CardActionArea>
          <CardContent>
            <Link
              underline='none'
              color='inherit'
              component={RouterLink}
              to={`/article/${article.id}`}
            >
              <Typography gutterBottom variant='h5' component='h2'>
                {article.title}
              </Typography>
              <Typography gutterBottom variant='subtitle2' component='h2'>
                Posted by {article.username} :{' '}
                {article.createdAt &&
                  moment(article.createdAt.toDate()).calendar()}
              </Typography>
              <Typography variant='body2' component='p'>
                {article.body}
              </Typography>
            </Link>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};

export default Article;

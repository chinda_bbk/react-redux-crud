import React, { FC, useState, useCallback, useEffect } from 'react';
import { IArticle } from '../../redux/article/article-type';
import { RootState } from '../../redux/root-reducer';
import { useSelector, useDispatch } from 'react-redux';
import { addArticle, receiveArticle } from '../../redux/article/article-action';

import Article from './Article';
import AddArticle from './AddArticle';
import Search from '../UI/Search';
import PaginateStepper from '../UI/PaginateStepper';
import Message from '../UI/Message';

import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const Articles: FC = () => {
  const dispatch = useDispatch();
  const { articles, searched } = useSelector(
    (state: RootState) => state.articles
  );
  const { authenticated } = useSelector((state: RootState) => state.auth);
  const [openModal, setOpenModal] = useState(false);
  const handleModal = () => setOpenModal(!openModal);
  
  const saveArticle = useCallback(
    (article: IArticle) => {
      dispatch(addArticle(article));
      dispatch(receiveArticle());
  }, [dispatch]);

  useEffect(() => {
    dispatch(receiveArticle());
  }, [dispatch]);

  return (
    <>
      <Typography variant='h4' gutterBottom className='Title'>
        Articles
        {authenticated && (
          <IconButton
            onClick={handleModal}
            color='inherit'
            className='Title-icon'
            style={{display: searched ? 'none' : 'flex'}}
          >
            <PlusOneIcon />
          </IconButton>
        )}
      </Typography>
      <Modal
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'
        open={openModal}
        onClose={handleModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <div>
          <AddArticle saveArticle={saveArticle} handleModal={handleModal} />
        </div>
      </Modal>
      
      <Box my={3}><Search /></Box>

      {searched && searched.length === 0 ? (
        <Typography color='inherit' align='center'>
          <Message msg='No results were returned. Please refine your search.' />
        </Typography>
      ) : (
        <>
          <Grid container spacing={2} key={articles ?articles.length : 0}>
            {searched !== null
              ? searched.map((article) => (
                  <Grid item key={article.id} xs={6}>
                    <Article article={article} />
                  </Grid>
                ))
              : articles &&
                articles.map((article) => (
                  <Grid item key={article.id} xs={6}>
                    <Article article={article} />
                  </Grid>
                ))}
          </Grid>
          <Box mb={5} mt={3} display={searched ? 'none' : 'flex'}>
            <PaginateStepper />
          </Box>
        </>
      )}
    </>
  );
};

export default Articles;

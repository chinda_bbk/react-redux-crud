import React, { FC, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/root-reducer';
import { CommentType, EditCommentData } from '../../redux/comment/comment-type';
import moment from 'moment';
import EditComment from './EditComment';

import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';

type Props = {
  comment: CommentType;
  articleId: string;
  delComment: (id: string) => void;
  updateComment: (comment: EditCommentData) => void;
};

const Comment: FC<Props> = ({
  comment,
  articleId,
  delComment,
  updateComment,
}) => {
  const { authenticated, user } = useSelector((state: RootState) => state.auth);
  const [owner, setOwner] = useState(false);
  useEffect(() => {
    if ((user && user.id) === (comment && comment.userId)) setOwner(true);
    else setOwner(false);
  }, [user, comment]);

  const getTime = moment(
    comment.createdAt && comment.createdAt.toDate()
  ).fromNow();
  const [showEdit, setShowEdit] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openMenu = Boolean(anchorEl);

  function handleDelete() {
    delComment(comment.id);
    setAnchorEl(null);
  }

  const handleMenuItem = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleShowEdit = () => {
    setShowEdit(!showEdit);
    setAnchorEl(null);
  };
  return (
    <Box py={1} my={1}>
      {!showEdit ? (
        <Box>
          <Box display='flex' justifyContent='space-between'>
            <Box>
              <Typography variant='body2' gutterBottom>
                <b>{comment.username}</b> {getTime && getTime}
              </Typography>
              <Typography variant='body2' gutterBottom>
                {comment.content}
              </Typography>
            </Box>

            {authenticated && owner && (
              <Box>
                <IconButton
                  aria-label='more'
                  aria-controls='long-menu'
                  aria-haspopup='true'
                  onClick={handleMenuItem}
                >
                  <MoreVertIcon />
                </IconButton>
              </Box>
            )}
          </Box>

          <Menu
            id='long-menu'
            anchorEl={anchorEl}
            keepMounted
            open={openMenu}
            onClose={handleCloseMenu}
          >
            <MenuItem onClick={handleShowEdit}>Edit</MenuItem>
            <MenuItem onClick={handleDelete}>Delete</MenuItem>
          </Menu>
        </Box>
      ) : (
        <Box my={5} key={comment.id}>
          <EditComment
            comment={comment}
            articleId={articleId}
            updateComment={updateComment}
            setShowEdit={setShowEdit}
          />
        </Box>
      )}
    </Box>
  );
};

export default Comment;

import React, { FC, useState } from 'react';
import { CommentType, EditCommentData } from '../../redux/comment/comment-type';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

type Props = {
  articleId: string;
  comment: CommentType;
  updateComment: (comment: EditCommentData) => void;
  setShowEdit: (show: boolean) => void;
};

const EditComment: FC<Props> = ({
  articleId,
  comment,
  updateComment,
  setShowEdit,
}) => {
  const [commentData, setCommentData] = useState<EditCommentData>({
    articleId: articleId,
    content: comment.content,
    id: comment.id,
  });
  const handleContent = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setCommentData({ ...commentData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    updateComment(commentData);
    setShowEdit(false);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <TextField
          onChange={handleContent}
          name='content'
          type='text'
          fullWidth
          value={commentData.content}
          multiline
        />
        <Box style={{ float: 'right' }} my={1}>
          <Button type='submit' color='primary'>
            Save
          </Button>
          <Button onClick={() => setShowEdit(false)}>Cancel</Button>
        </Box>
      </form>
    </div>
  );
};

export default EditComment;

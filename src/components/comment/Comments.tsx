import React, { FC, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  setComments,
  addComment,
  editComment,
  removeComment,
} from '../../redux/comment/comment-action';
import {
  AddCommentData,
  EditCommentData,
} from '../../redux/comment/comment-type';
import { RootState } from '../../redux/root-reducer';

import AddComment from './AddComment';
import Comment from './Comment';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

type Props = {
  articleId: string;
};

const Comments: FC<Props> = ({ articleId }) => {
  const dispatch = useDispatch();
  const { comments } = useSelector((state: RootState) => state.comments);
  const { authenticated } = useSelector((state: RootState) => state.auth);

  useEffect(() => {
    dispatch(setComments(articleId));
  }, [dispatch, articleId]);

  const saveComment = useCallback(
    (comment: AddCommentData) => {
      dispatch(addComment(comment));
    },
    [dispatch]
  );

  const updateComment = useCallback(
    (comment: EditCommentData) => {
      dispatch(editComment(comment));
    },
    [dispatch]
  );

  const delComment = useCallback(
    (id: string) => {
      dispatch(removeComment({ id, articleId }));
    },
    [dispatch, articleId]
  );

  return (
    <div>
      {authenticated && (
        <AddComment articleId={articleId} saveComment={saveComment} />
      )}
      <div>
        <Box mt={3}>
          <Typography variant='subtitle2' gutterBottom>
            {comments && comments.length} Comments
          </Typography>
        </Box>
        {comments &&
          comments.map((comment) => (
            <Comment
              key={comment.id}
              articleId={articleId}
              comment={comment}
              delComment={delComment}
              updateComment={updateComment}
            />
          ))}
      </div>
    </div>
  );
};

export default Comments;

import React, { FC, useState } from 'react';
import { RootState } from '../../redux/root-reducer';
import { useSelector } from 'react-redux';
import { AddCommentData } from '../../redux/comment/comment-type';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

type Props = {
  articleId: string;
  saveComment: (comment: AddCommentData | any) => void;
};

const AddComment: FC<Props> = ({ articleId, saveComment }) => {
  const user = useSelector((state: RootState) => state.auth.user);
  const [commentData, setCommentData] = useState<AddCommentData>({
    content: null!,
    username: user.firstName,
    articleId: articleId,
  });

  const handleContent = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setCommentData({ ...commentData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    saveComment(commentData);
  };

  return (
    <Box my={3}>
      <form onSubmit={handleSubmit}>
        <TextField
          multiline
          onChange={handleContent}
          name='content'
          placeholder='Add a comment...'
          fullWidth={true}
        />
        <Box style={{ float: 'right' }} my={1}>
          <Button
            type='submit'
            disabled={commentData.content ? false : true}
            color='primary'
          >
            Comment
          </Button>
        </Box>
      </form>
    </Box>
  );
};

export default AddComment;

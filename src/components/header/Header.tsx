import React, { FC } from 'react'
import logo from '../../image/logo.svg'
import { Link, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../redux/root-reducer'
import { signout } from '../../redux/user-auth/auth-action'
import { resetSearch } from '../../redux/article/article-action'

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const Header: FC = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const { authenticated } = useSelector((state: RootState) => state.auth)

  const logoutClickHandler = () => {
    dispatch(signout())
  }

  return (
    <header>
      <AppBar color="default">
        <Toolbar className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Typography variant="h5" className="App-content" >
            Articlist Redux Typscript
          </Typography>
          <Grid container justify="flex-end" spacing={2}>
            <Grid item >
              <Button  component={Link} to={!authenticated ? "/articles" : "/articles"} color="inherit" onClick={() => dispatch(resetSearch())}>Articlist</Button>
            </Grid>
            {!authenticated ?
                <Grid item>
                  <Button size="small" variant="contained" color="primary" onClick={() => history.push('/signin')} >Sign in</Button>
                </Grid>
              : <>
              <Grid item ><Button color="inherit" onClick={() => history.push('/profile')} >Profile</Button></Grid>
              <Grid item ><Button variant="contained" color="primary" onClick={logoutClickHandler}>Sign out</Button></Grid>            
              </>
            }
          </Grid>
        </Toolbar>
      </AppBar>
    </header>
  )
}

export default Header

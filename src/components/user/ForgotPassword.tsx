import React, { FC, useState, useEffect, FormEvent } from 'react';
import { useDispatch, useSelector} from 'react-redux'
import { RootState } from '../../redux/root-reducer'
import { sendPasswordResetEmail, setError } from '../../redux/user-auth/auth-action'
import { Link } from 'react-router-dom'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
// import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';

const ForgotPassword: FC = () => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch()
  const { error, success } = useSelector((state: RootState) => state.auth)
  console.log(success, error)
  useEffect(() => {
    console.log('call after render')
    return () => {
      error && dispatch(setError(''))
    }
  }, [error, success, dispatch]);

  const submitHandler = async (e: FormEvent) => {
    e.preventDefault()
    setLoading(true)
    await dispatch(sendPasswordResetEmail(email, "Email sent! Please check you email to reset your password."))
    setLoading(false)
  }

  if(success) {
    return (
      <div>
      <Typography variant="caption" display="block" gutterBottom className="Success">
        {success} &nbsp;
        <Link to="/signin" color="inherit" className="Form-link" >Sign in Again?</Link>
      </Typography>
      </div>
    )
  }

  return (
    <div className="Form-wrapper">
      <form onSubmit={submitHandler} className="Form">
        <Typography variant="h5">Reset password</Typography>
        <Typography>Enter your fogot password account</Typography>
        {console.log('rendering')}
        {error && 
          <Typography variant="caption" display="block" gutterBottom className="Error">
            {error}
          </Typography>
        }
        <TextField
          autoFocus
          variant='outlined'
          type='email'
          label='Email address'
          name='Email address'
          value={email}
          onChange={(e) => setEmail(e.currentTarget.value)}
        />
        <div>
          <Button type='submit' variant="contained" color="primary" disabled={loading} >
            {loading ? 'Loading...' : 'Send password reset email'}
          </Button>
        </div>
      </form>
    </div>
  )
}

export default ForgotPassword;
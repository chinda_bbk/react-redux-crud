import React, { FC, useState, useEffect, FormEvent } from 'react';
import { useDispatch, useSelector} from 'react-redux'
import { RootState } from '../../redux/root-reducer'
import { signin, setError } from '../../redux/user-auth/auth-action'
import { Link } from 'react-router-dom'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const SignIn: FC = () => {
  const dispatch = useDispatch()
  const { error } = useSelector((state: RootState) => state.auth)
  const [email, setEmail] = useState('');
  // const [emailError, setEmailError] = useState('');
  const [password, setPassword] = useState('');
  // const [passwordError, setPasswordError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    return () => {
      if(error) {
        dispatch(setError(''))
      }
    }
  }, [error, dispatch]);

  const submitHandler = (e: FormEvent) => {
    e.preventDefault()
    if(!email) {
      dispatch(setError('Enter an email address'))
    } else {
      if(error) {
        dispatch(setError(''))
      }
      setLoading(true)
      dispatch(signin({ email, password }, () => setLoading(false)))
    }
  }

  return (
    <div className="Form-wrapper">
      <form onSubmit={submitHandler} className="Form">
        <Typography variant="h4">Sign in</Typography>
        <Typography>Use your Fakerbase Account</Typography>
        {error && 
          <Typography variant="caption" display="block" gutterBottom color='secondary' className="Error">
            {error}
          </Typography>
        }
        <TextField
          focused
          variant='outlined'
          type='email'
          label='Email address'
          name='Email address'
          value={email}
          onChange={(e) => setEmail(e.currentTarget.value)}
        />
        <TextField
          variant='outlined'
          type='password'
          label='Password'
          name='password'
          value={password}
          onChange={(e) => setPassword(e.currentTarget.value)}
        />
        <Link to='/forgot-password' className="Form-link"><Typography align="left" color="primary">Forgot Password?</Typography></Link>
        <div className="Form-action">
          <Typography>Don't have an account? {' '} <Link to='/signup' className="Form-link">Sign up</Link></Typography>
          <Button type='submit' variant="outlined" color="primary" disabled={loading} >
            {loading ? 'Loading...' : 'Sign in'}
          </Button>
        </div>
      </form>
    </div>
  )
}

export default SignIn;
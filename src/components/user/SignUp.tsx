import React, { FC, useState, useEffect, FormEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../redux/root-reducer'
import { signup, setError } from '../../redux/user-auth/auth-action'
import { Link } from 'react-router-dom'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
// import ErrorIcon from '@material-ui/icons/Error';
import Typography from '@material-ui/core/Typography';

const SingUp: FC = () => {
  const dispatch = useDispatch()
  const { error } = useSelector((state: RootState) => state.auth)
  const [firstName, setFirstName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    return () => {
      if (error) {
        dispatch(setError(''))
      }
    }
  }, [error, dispatch]);

  const submitHandler = (e: FormEvent) => {
    e.preventDefault()
    if (error) {
      dispatch(setError(''));
    }
    setLoading(true)
    dispatch(signup({ email, password, firstName }, () => setLoading(false)))
  }

  return (
    <div className="Form-wrapper">
      <form onSubmit={submitHandler} className="Form">
        <Typography variant="h4">Sign up</Typography>
        <Typography>Use your Fakerbase Account</Typography>
        {error &&
          <Typography variant="caption" display="block" gutterBottom color='secondary' className="Error">
            {error}
          </Typography>
        }
        <TextField
          focused
          variant='outlined'
          type='text'
          label='User name'
          name='User name'
          value={firstName}
          onChange={(e) => setFirstName(e.currentTarget.value)}
        />
        <TextField
          variant='outlined'
          type='text'
          label='Email address'
          name='Email address'
          value={email}
          onChange={(e) => setEmail(e.currentTarget.value)}
        />
        <TextField
          variant='outlined'
          type='password'
          label='Password'
          name='password'
          value={password}
          onChange={(e) => setPassword(e.currentTarget.value)}
        />
        <div className="Form-action">
          <Typography>Have an account? {' '} <Link to="/signin" color="inherit" className="Form-link" >Sign in</Link></Typography>
          <Button type='submit' variant="outlined" color="primary" disabled={loading} >
            {loading ? 'Loading...' : 'Sign up'}
          </Button>
        </div>
      </form>
    </div>
  );
}

export default SingUp;

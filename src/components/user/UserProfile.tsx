import React, { FC, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../redux/root-reducer'
import { setSuccess } from '../../redux/user-auth/auth-action'
import Message from '../UI/Message'

import Typography from '@material-ui/core/Typography';

const UserProfile: FC = () => {
  const authState  = useSelector((state: RootState) => state.auth)
  const dispatch = useDispatch()
  const { user, needVerification, success } = authState

  useEffect(() => {
    if(success) {
      dispatch(setSuccess(''))
    }
  }, [success, dispatch])

  return (
    <div>
      <Typography variant="h3" align="center" gutterBottom>
        Welcome { user?.firstName }
      </Typography>
      {needVerification && <Typography color="secondary" align="center">
        <Message msg="Please verify your email address." />
        </Typography>
      }
    </div>
  )
}

export default UserProfile

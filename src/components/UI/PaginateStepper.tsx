import React from 'react';
import { RootState } from '../../redux/root-reducer';
import { useSelector, useDispatch } from 'react-redux';
import {
  getNextArticle,
  getPrevArticle,
} from '../../redux/article/article-action';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const useStyles = makeStyles({
  root: {
    maxWidth: '100%',
    flexGrow: 1,
  },
});

export default function ProgressMobileStepper() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const { lastArticle, firstArticle, nextStep, prevStep } = useSelector(
    (state: RootState) => state.articles
  );

  const handleNext = () => {
    if (lastArticle) dispatch(getNextArticle(lastArticle));
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };
  const handlePrev = () => {
    if (firstArticle) dispatch(getPrevArticle(firstArticle));
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <MobileStepper
      variant='progress'
      steps={6}
      position='static'
      activeStep={activeStep}
      className={classes.root}
      nextButton={
        <Button size='small' onClick={handleNext} disabled={nextStep}>
          Next
          {theme.direction === 'rtl' ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </Button>
      }
      backButton={
        <Button size='small' onClick={handlePrev} disabled={prevStep}>
          {theme.direction === 'rtl' ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
          Back
        </Button>
      }
    />
  );
}

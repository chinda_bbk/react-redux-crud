import React, { FC } from 'react';
import { RootState } from '../../redux/root-reducer';
import { useSelector } from 'react-redux';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      minWidth: 120,
    },
    selectEmpty: {
      height: 35,
      "&:focus": {
        backgroundColor: "none"
      }
    },
  }),
);

type Props = { setUsername: (name: string) => void }

const SimpleSelect: FC<Props> = ({setUsername}) => {
  const classes = useStyles();
  const { users } = useSelector((state: RootState) => state.articles);
  const { user } = useSelector((state: RootState) => state.auth);
  const [name, setName] = React.useState(user?user.firstName:'');

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setName(event.target.value as string);
  };

  React.useEffect(() => {
    setUsername(name);
  }, [setUsername, name])

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={name}
          onChange={handleChange}
          className={classes.selectEmpty}
        >
          {users&& users.map((user, index) => (
            <MenuItem key={index} value={user}>{user}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}

export default SimpleSelect;
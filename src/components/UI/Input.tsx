import React, { FC, InputHTMLAttributes } from 'react'
// import TextField from '@material-ui/core/TextField';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string
  variant: "standard" | "filled" | "outlined"
  type?: string
  name?: string
  value?: string | number
}

const Input: FC<InputProps> = ({ label, variant, type, name, value }) => {
  return (
    // <TextField
    //   variant={variant}
    //   type={type}
    //   label={label}
    //   name={name}
    //   value={value}
    // />
    <div></div>
  )
}

export default Input
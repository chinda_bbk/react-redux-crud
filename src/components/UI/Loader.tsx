import React, { FC } from 'react'
import logo from '../../image/logo.svg'

const Loader: FC = () => {
  return (
    <div className="Loader">
      <img src={logo} className="Loader-logo" alt="logo" />
    </div>
  )
}

export default Loader
import React, { FC } from 'react'

interface MessageProps {
  msg: string
}

const Message: FC<MessageProps> = ({ msg }) => {
  return (
    <>{msg}</>
  )
}

export default Message
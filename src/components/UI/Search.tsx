import React, { useState } from 'react';
import { Dispatch } from 'redux';
import { useDispatch } from 'react-redux';
import { searchArticle, getUsers } from '../../redux/article/article-action';
import Select from './Select'

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      maxWidth: '100%',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 150,
    },
  })
);

export default function Search() {
  const classes = useStyles();
  const dispatch: Dispatch<any> = useDispatch();
  const [input, setInput] = useState('');
  const [name, setName] = useState('');
  function handleInput(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    setInput(e.currentTarget.value);
  }
  const [dateStart, setDateStart] = useState('2020-09-01');
  const [dateEnd, setDateEnd] = useState('2020-12-30');
  function handleInputDateStart(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    setDateStart(e.currentTarget.value);
  }
  function handleInputDateEnd(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) {
    setDateEnd(e.currentTarget.value);
  }
  function handleSubmit(e: React.FormEvent<HTMLDivElement>) {
    e.preventDefault();
    if (input !== '') dispatch(searchArticle(input, dateStart, dateEnd, name));
  }

  React.useEffect(() => {
    dispatch(getUsers())
  }, [dispatch])

  return (
    <>
      <Paper component='form' className={classes.root} onSubmit={handleSubmit}>
        <InputBase
          className={classes.input}
          placeholder='Search by title'
          onChange={handleInput}
          value={input}
        />
        <IconButton type='submit' className={classes.iconButton}>
          <SearchIcon />
        </IconButton>
        <Divider className={classes.divider} orientation='vertical' />
        <Typography variant='subtitle2'>
          <span style={{ color: '#00d1b2' }}>Searching for: </span>
          {input}
          <span style={{ color: '#00d1b2' }}> from:</span>
        </Typography>
        <TextField
          id='date'
          type='date'
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputDateStart}
          value={dateStart}
        />
        <Typography variant='subtitle2'>
          <span style={{ color: '#00d1b2' }}> to:</span>
        </Typography>
        <TextField
          id='date'
          type='date'
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleInputDateEnd}
          value={dateEnd}
        />
        <Typography variant='subtitle2'>
          <span style={{ color: '#00d1b2' }}> by:</span>
        </Typography>
        <Select setUsername={setName} />
      </Paper>
    </>
  );
}

import React, { FC, useEffect } from 'react';
import firebase from './firebase/config'
import { useSelector, useDispatch } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { RootState } from './redux/root-reducer'
import { getUserById, setLoading, setNeedVerification } from './redux/user-auth/auth-action'
import Loader from './components/UI/Loader'
import './App.css';

import PublicRoute from './routes/PublicRoute'
import PrivateRoute from './routes/PrivateRoute'
import Header from './components/header/Header'
import Articles from './components/article/Articles'
import SingleArticle from './components/article/SingleArticle'
import SignUp from './components/user/SignUp'
import SignIn from './components/user/SignIn'
import ForgotPassword from './components/user/ForgotPassword';
import UserProfile from './components/user/UserProfile';

const App: FC = () => {
  const authState  = useSelector((state: RootState) => state.auth)
  const dispatch = useDispatch()
  const { loading, success } = authState

  // Check if user exists
  useEffect(() => {
    dispatch(setLoading(true))
    const unsubscribe = firebase.auth().onAuthStateChanged(async (user) => {
      if(user) {
        dispatch(setLoading(true))
        await dispatch(getUserById(user.uid))
        if(!user.emailVerified) {
          dispatch(setNeedVerification())
        }
      }
      dispatch(setLoading(false))
    })
    return () => unsubscribe()
  }, [success, dispatch])

  if(loading){
    return <Loader />
  } 

  return (
    <div className="App">
      <Router>
        <Header />
        <Route exact path="/" component={Articles} />
        <Route exact path="/articles" component={Articles} />
        <Route exact path="/article/:id" component={SingleArticle} />
        <Switch>
          <PublicRoute exact path="/signup" component={SignUp} />
          <PublicRoute exact path="/signin" component={SignIn} />
          <PublicRoute exact path="/forgot-password" component={ForgotPassword} />
          <PrivateRoute exact path="/profile" component={UserProfile} />
        </Switch>
      </Router>
    </div>
  );
}

export default App
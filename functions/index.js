const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// http request
exports.testFunc = functions.https.onRequest((request, response) => {
  response.send("Test");
  // response.redirect('https://www.youtube.com/');
});

// http callable function
exports.sayHello = functions.https.onCall((data, context) => {
  return `hello, ${data}`
})

// Notification
const createNotification = (async notification => {
  const doc = await admin.firestore().collection('notifications')
    .add(notification);
  return console.log('notification added', doc);
})

// Firestore trigger
// exports.logActivities = functions.firestore.document('/{collection}/{id}').onCreate((snap, context) => {
//   console.log(snap.data());
//   const collection = context.params.collection;
//   const id = context.params.id;

//   const activities = admin.firestore.collection('activities');
//   if(collection === 'requests') {
//     return activities.add({text: 'new request'})
//   }
//   if(collection === 'users') {
//     return activities.add({text: 'new user'})
//   }
//   return null;
// })

exports.articleCreated = functions.firestore
  .document('articles/{articleId}')
  .onCreate((doc, context) => {
    const article = doc.data();
    const notification = {
      content: 'Added a new article',
      user: `${article.username}`,
      articleId: `${article.id}`,
      userId: `${article.userId}`,
      time: admin.firestore.FieldValue.serverTimestamp()
    }
    return createNotification(notification);
  })

// LogsRecord
const createLogsDbRecords = (async (collValue, msg) => {
  await admin.firestore().collection('LogsDbRecords')
    .add(collValue);
  return console.log(msg);
})

// Article Delete
exports.onDeleteArticle = functions.firestore
  .document('articles/{articleId}')
  .onDelete((snap, context) => {
    const article = snap.data();
    const articleValue = {
      ...article,
      type: 'DELETE',
      deleteAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'articles',
    }

    return createLogsDbRecords(articleValue, 'deleted article');
  })

// Comment Delete
exports.onDeleteComment = functions.firestore
  .document('articles/{articleId}/comments/{commentId}')
  .onDelete(async (snap, context) => {
    const comment = snap.data();
    const commentValue = {
      ...comment,
      type: 'DELETE',
      deleteAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'comments',
    }

    // Count comment 
    try {
      await admin.firestore().collection('articles').where('id', '==', comment.articleId).get().then((snaper) => {
        snaper.forEach((doc) => {
          const artSnap = doc.data()
          // Update number of comment
          doc.ref.update({ comment: artSnap.comment - 1 })
        });
      });
    } catch (err) {
      console.log(err);
    }

    return createLogsDbRecords(commentValue, 'deteted comment');
  })

// User Delete
exports.onDeleteUser = functions.firestore
  .document('users/{userId}')
  .onDelete((snap, context) => {
    const user = snap.data();
    const userValue = {
      ...user,
      type: 'DELETE',
      deleteAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'users',
    }

    return createLogsDbRecords(userValue, 'deteted user');
  })

// Article Update
exports.onUpdateArticle = functions.firestore
  .document('articles/{articleId}')
  .onUpdate((snap, context) => {
    const article = snap.before.data();
    const articleValue = {
      ...article,
      type: 'UPDATE',
      updateAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'articles',
    }

    return createLogsDbRecords(articleValue, 'updated article');
  })

// Comment Update
exports.onUpdateComment = functions.firestore
  .document('articles/{articleId}/comments/{commentId}')
  .onUpdate((snap, context) => {
    const comment = snap.before.data();
    const commentValue = {
      ...comment,
      type: 'UPDATE',
      updateAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'comments',
    }

    return createLogsDbRecords(commentValue, 'updated comment');
  })

// User Update
const updateAllColls = (async (user) => {
  const db = admin.firestore();
  let batch = db.batch();
  let artRef = db.collection('articles').where('userId', '==', user.id);
  let cmtRef = db.collection('articles/{articleId}/comments').where('userId', '==', user.id);
  let ntfRef = db.collection('notifications').where('userId', '==', user.id);
  try {
    // Update articles
    const artSnap = await artRef.get();
    artSnap.forEach((doc) => {
      batch.update(doc.ref, { username: user.firstName });
    });
    // Update comments
    const cmtSnap = await cmtRef.get();
    cmtSnap.forEach((doc) => {
      batch.update(doc.ref, { username: user.firstName });
    });
    const ntfSnap = await ntfRef.get();
    // Update notifications
    ntfSnap.forEach((doc) => {
      batch.update(doc.ref, { user: user.firstName });
    });
    return batch.commit();
  } catch (err) {
    console.log('error=>', err);
  }
})

exports.onUpdateUser = functions.firestore
  .document('users/{userId}')
  .onUpdate(async (snap, context) => {
    const userAfter = snap.after.data();
    const userBefore = snap.before.data();
    const userValue = {
      ...userBefore,
      type: 'UPDATE',
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
      collection: 'users',
    }

    await updateAllColls(userAfter);
    return createLogsDbRecords(userValue, 'updated user');
  })

// Counting Comment
exports.onCommentCreated = functions.firestore
  .document('articles/{articleId}/comments/{commentId}')
  .onCreate(async (snap, context) => {
    try {
      const cmtRef = snap.data();
      await admin.firestore().collection('articles').where('id', '==', cmtRef.articleId).get().then((snaper) => {
        snaper.forEach((doc) => {
          const artSnap = doc.data()
          // Update number of comment
          doc.ref.update({ comment: artSnap.comment + 1 })
        });
      });

      return console.log('Comment added');
    } catch (err) {
      console.log(err);
    }
  })